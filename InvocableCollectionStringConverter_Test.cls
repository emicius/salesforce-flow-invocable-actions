@IsTest
public with sharing class InvocableCollectionStringConverter_Test {
    
    private static Map<String, List<String>> testStrings = new Map<String, List<String>>{
        'one;two;three' => new List<String>{'one,two,three', ','},
        'semicolon;test;one;two' => new List<String>{'semicolon;test;one;two', ';'},
        'three;two;one' => new List<String>{'three|two|one', '|'},
        'more;regex;breaking' => new List<String>{'more.regex.breaking', '.'},
        'this,will,not,get,split' => new List<String>{'this,will,not,get,split', ';'}
    };

    @IsTest
    private static void converStringToCollectionTest(){
        List<InvocableCollectionStringConverter.Request> requests = new List<InvocableCollectionStringConverter.Request>();
        
        for(String s : testStrings.keySet()){
            InvocableCollectionStringConverter.Request request = new InvocableCollectionStringConverter.Request();
            request.delimitedString = testStrings.get(s)[0];
            request.delimiter = testStrings.get(s)[1];
            requests.add(request);
        }
        
        List<InvocableCollectionStringConverter.Result> results = new List<InvocableCollectionStringConverter.Result>();

        Test.startTest();
            results = InvocableCollectionStringConverter.convert(requests);
        Test.stopTest();

        for(InvocableCollectionStringConverter.Result r : results){
            System.assert(r.isSuccess, 'Conversion has failed');
            System.assert(testStrings.keySet().contains(String.join(r.stringCollection, ';')) , 'Delimited String ' + String.join(r.stringCollection, ';') + ' was not converted correctly');
        }
        
    }

    @IsTest
    private static void converCollectionToStringTest(){

        List<InvocableCollectionStringConverter.Request> requests = new List<InvocableCollectionStringConverter.Request>();
        
        for(String s : testStrings.keySet()){
            InvocableCollectionStringConverter.Request request = new InvocableCollectionStringConverter.Request();
            request.stringCollection = s.split(';');
            request.delimiter = testStrings.get(s)[1];
            requests.add(request);
        }

        List<InvocableCollectionStringConverter.Result> results = new List<InvocableCollectionStringConverter.Result>();

        Test.startTest();
            results = InvocableCollectionStringConverter.convert(requests);
        Test.stopTest();

        for(InvocableCollectionStringConverter.Result r : results){
            System.assert(r.isSuccess, 'Conversion has failed');
            System.assertEquals(testStrings.get(String.join(r.stringCollection, ';'))[0], r.delimitedString, 'Collection ' + String.join(r.stringCollection, ';') + ' was not converted correctly');
        }
    }

    @IsTest
    private static void emptyInputTest(){
        List<InvocableCollectionStringConverter.Request> requests = new List<InvocableCollectionStringConverter.Request>();
        InvocableCollectionStringConverter.Request request = new InvocableCollectionStringConverter.Request();
        request.delimitedString = '';
        request.delimiter = '';
        request.stringcollection = new List<String>();
        requests.add(request);

        List<InvocableCollectionStringConverter.Result> results = new List<InvocableCollectionStringConverter.Result>();

        Test.startTest();
            results = InvocableCollectionStringConverter.convert(requests);
        Test.stopTest();

        for(InvocableCollectionStringConverter.Result r : results){
            System.assert(!r.isSuccess, 'Conversion was not supposed to succeed!');
        }

    }
}