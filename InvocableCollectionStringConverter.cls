/**
 * Invocable action to convert delimited String to String Collection variable and vice-versa
 *
 * @author : Evaldas M.
 *
 * @group : Invocable Apex
 * @last modified on  : 2023-05-09
 * @last modified by  : Evaldas M.
 **/
public with sharing class InvocableCollectionStringConverter {

    /**
     * Input request class to group all inputs for bulkification purpose
     */
    public class Request {
        @InvocableVariable(Label='Delimited String' Description='Delimited string to convert to a String Collection variable')
        public String delimitedString = '';
        @InvocableVariable(Label='String Collection' Description='String Collection to convert to a delimited String')
        public List<String> stringCollection = new List<String>();
        @InvocableVariable(Label='Delimiter (default - semicolon ;)' Description='Value delimiter. Defaults to semicolon ;')
        public String delimiter = ';';
    }

    /**
     * Output result class to group all outputs for bulkification purpose
    */
    public class Result {
        @InvocableVariable(Label='Is Success' Description='Was action performed successfully?')
        public Boolean isSuccess;
        @InvocableVariable(Label='Status Message' Description='"SUCCESS" when isSuccess is true, otherwise the encountered error message')
        public String message;
        @InvocableVariable(Label='Resulting delimited string' Description='Delimited string resulting from joined collection variables')
        public String delimitedString;
        @InvocableVariable(Label='Resulting String Collection' Description='Collection variable resulting from splitting delimited string to individual elements')
        public String[] stringCollection;
    
        public Result() {
            this.isSuccess = true;
            this.message = 'SUCCESS';
            this.delimitedString = '';
            this.stringCollection = new List<String>();
        }
    }

    /**
     * Description Invocable Apex action to convert String Collection variable to a delimited string and vice versa
     * 
     * @param requests List<InvocableCollectionStringConverter.Request> List of parameters for Collection <-> Delimited String conversion
     * 
     * @return List<InvocableCollectionStringConverter.Results> List of converted params - outputs both delimited string and collection
     */
    @InvocableMethod(Label='Convert Collection <-> String' Description='Converts String Collection variable to delimited string and vice-versa' Category='Data Management')
    public static List<InvocableCollectionStringConverter.Result> convert(List<InvocableCollectionStringConverter.Request> requests) {
        List<InvocableCollectionStringConverter.Result> results = new List<InvocableCollectionStringConverter.Result>();

        for (Request request : requests) {
            InvocableCollectionStringConverter.Result result = new InvocableCollectionStringConverter.Result();

            if (delimitedStringProvided(request)) {
                result.stringCollection = convertToCollection(request.delimitedString, request.delimiter);
                result.delimitedString = request.delimitedString;
            } else if (collectionProvided(request)){
                result.stringCollection = request.stringCollection;
                result.delimitedString = convertToDelimitedString(request.stringCollection, request.delimiter);
            } else {
                result.isSuccess = false;
                result.message = 'Both input variables were empty';
            }
            results.add(result);
        }
        return results;
    }

    private static Boolean delimitedStringProvided(InvocableCollectionStringConverter.Request request){
        return !String.isBlank(request.delimitedString);
    }

    private static Boolean collectionProvided(InvocableCollectionStringConverter.Request request){
        return !request.stringCollection.isEmpty();
    }

    private static List<String> convertToCollection(String delimitedString, String delimiter){
        return delimitedString.split('\\' + delimiter);
    }

    private static String convertToDelimitedString(List<String> stringCollection, String delimiter){
        return String.join(stringCollection, delimiter);
    }
}